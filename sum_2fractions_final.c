#include<stdio.h>
#include<stdlib.h>
struct fraction
{
    int num,den;
}f[2],r;
void inp(struct fraction f[])
{
    int i;
    printf("Enter the fractions:\n");
    for(i=0;i<2;i++)
    {
        printf("%d. ",i+1);
        scanf("%d/%d",&f[i].num,&f[i].den);
    }


}
int lcm1(int a, int b)
{
    int g,lcm;
    if(a>b)
        g=a;
    else g=b;
    while(g<=a*b)
    {
        if(g%a==0 && g%b==0 )
            {
                lcm=g;
                break;}
        else g++;
    }
    return lcm;
}
void fra_check(struct fraction f[])
{
    int i, flag=0;
    char c;
    for(i=0;i<2;i++)
    {
        if(f[i].den==0)
         {
            printf("\nError in evaluating %d/%d", f[i].num,f[i].den);
            flag++;
            printf("\nDo you want to enter new set of fractions?(y/n):");
            scanf(" %c", &c);
            if(c=='y'|| c=='Y')
             {
                struct fraction f[2], r;
                inp(f);
                fra_check(f);
                comp(f,&r);
                disp(r,f);
             }
        else exit(0);
         }
    }
    if(flag!=0)
    exit(0);
    
}
void comp(struct fraction f[], struct fraction *r)
{
    int i,g,lcm,h=1;
    g=f[0].den;
    for(i=1;i<2;i++)
    {
        if(f[i].den>g)
            g=f[i].den;
    }
    for(h=1,i=0;i<2;h*=f[i].den,i++);
    lcm=g;
    for(i=0;i<2;i++)
        {
            lcm=lcm1(lcm,f[i].den);
        }
    r->num=0;
    r->den=lcm;
    for(i=0;i<2;i++)
    {
        r->num+=(f[i].num)*(lcm/f[i].den);
    }
    if (r->num>r->den)
        g=r->num;
    else g=r->den;
    for(i=g;i>=1;i--)
    {
        if(r->num%i==0 && r->den%i==0)
        {
            r->num=r->num/i;
            r->den=r->den/i;
            break;
        }
    }
}

void disp(struct fraction r,struct fraction f[])
{
    int i;
    for(i=0;i<2;i++)
    {
        printf("%d/%d + ",f[i].num,f[i].den);
    }
    printf("\b\b = ");
    if(r.den!=1)
    printf("%d/%d", r.num, r.den);
     else printf("%d",r.den);
}
int main()
{
    struct fraction f[2], r;
    inp(f);
    fra_check(f);
    comp(f,&r);
    disp(r,f);
    return 0;
}